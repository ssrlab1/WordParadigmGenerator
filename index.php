<?php
	header("Content-Type: text/html; charset=utf-8");
	$ini = parse_ini_file('service.ini');
	include_once 'WordParadigmGenerator.php';
	$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
	$languages = WordParadigmGenerator::loadLanguages();
	WordParadigmGenerator::loadLocalization($lang);
?>
<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
	<head>
		<title><?php echo WordParadigmGenerator::showMessage('title'); ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href='css/theme.css'>
		<link rel="icon" type="image/x-icon" href="img/favicon.ico">
		<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
		<?php include_once 'analyticstracking.php'; ?>
		<script>
			var inputTextGeneral = "<?php echo str_replace("\"", '\"', str_replace("\n", '\\n', WordParadigmGenerator::showMessage('default input general'))); ?>";
			var inputTextNooj = "<?php echo str_replace("\"", '\"', str_replace("\n", '\\n', WordParadigmGenerator::showMessage('default input nooj'))); ?>";
			var inputTextDefault = inputTextGeneral;
			$(document).ready(function() {
				$('#categoryId').change(function(){
					document.getElementById('tagId').value = '';
				});
				$("input:radio[name=mode]").change(function(){
					if($(this).val() == 'general') {
						$('#inputTextId').html(inputTextGeneral);
						inputTextDefault = inputTextGeneral;
					}
					else if($(this).val() == 'nooj') {
						$('#inputTextId').html(inputTextNooj);
						inputTextDefault = inputTextNooj;
					}
				});
				$('button#MainButtonId').click(function() {
					$('#resultBlockId').show('slow');
					$('#resultId').empty();
					$('#resultId').prepend($('<img>', { src: "img/loading.gif"}));
					$.ajax({
						type: 'POST',
						url: 'https://corpus.by/WordParadigmGenerator/api.php',
						data: {
							'localization': '<?php echo $lang; ?>',
							'text': $('textarea#inputTextId').val(),
							'mode': $("input:radio[name=mode]:checked").val(),
							'tag': $("input:text[name=tag]").val(),
							'category': $('select#categoryId').val()
						},
						success: function(msg) {
							var result = jQuery.parseJSON(msg);
							$('#resultId').html(result.result);
						},
						error: function() {
							$('#resultId').html('ERROR');
						}
					});
				});
			});
		</script>
	</head>
	<body>
		<!-- Novigation -->
		<nav class="navbar navbar-default">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="/"><img style="max-width:150px; margin-top: -7px;" src="img/main-logo.png" alt=""></a>
				</div>
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li class="service-name"><a href="/WordParadigmGenerator/?lang=<?php echo $lang; ?>"><?php echo WordParadigmGenerator::showMessage('title'); ?></a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li class="service-name"><a href="<?php echo WordParadigmGenerator::showMessage('help'); ?>" target="_blank">?</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo WordParadigmGenerator::showMessage($lang); ?><span class="caret"></span></a>
							<ul class="dropdown-menu">
								<?php
									$languages = WordParadigmGenerator::loadLanguages();
									foreach($languages as $language) {
										echo "<li><a href='?lang=$language'>" . WordParadigmGenerator::showMessage($language) . "</a></li>";
									}
								?>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<!-- End of Novigation -->
		<div class="container theme-showcase" role="main">
			<div class="row">
				<div class="col-md-12">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="btn-group pull-right">
									<button type="button" class="btn btn-default btn-xs" onclick="document.getElementById('inputTextId').value=inputTextDefault;"><?php echo WordParadigmGenerator::showMessage('refresh'); ?></button>
									<button type="button" class="btn btn-default btn-xs" onclick="document.getElementById('inputTextId').value='';"><?php echo WordParadigmGenerator::showMessage('clear'); ?></button>
								</div>
								<h3 class="panel-title"><?php echo WordParadigmGenerator::showMessage('input'); ?></h3>
							</div>
							<div class="panel-body">
								<p><textarea class="form-control" rows="10" id = "inputTextId" name="inputText" placeholder="Enter text"><?php echo str_replace('\n', "\n", WordParadigmGenerator::showMessage('default input general')); ?></textarea></p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-6">
						<input type="radio" name="mode" value="general" id="general" checked>
						<label for="general">
							<?php echo WordParadigmGenerator::showMessage('general mode'); ?>
						</label><br />
						<input type="radio" name="mode" value="nooj" id="nooj">
						<label for="nooj">
							<?php echo WordParadigmGenerator::showMessage('nooj mode'); ?>
						</label><br />
					</div>
					<div class="col-md-6">
						<label for="tagId"><?php echo WordParadigmGenerator::showMessage('tag'); ?></label>
						<input type="text" id="tagId" name="tag"  size="15" value="" class="input-primary">
						<select id="categoryId" name="category" class="selector-primary">
							<option value='усе' selected><?php echo WordParadigmGenerator::showMessage('all parts-of-speech'); ?></option>
							<option value='назоўнік'><?php echo WordParadigmGenerator::showMessage('noun'); ?></option>
							<option value='прыметнік'><?php echo WordParadigmGenerator::showMessage('adjective'); ?></option>
							<option value='лічэбнік'><?php echo WordParadigmGenerator::showMessage('numeral'); ?></option>
							<option value='займеннік'><?php echo WordParadigmGenerator::showMessage('pronoun'); ?></option>
							<option value='дзеяслоў'><?php echo WordParadigmGenerator::showMessage('verb'); ?></option>
							<option value='прыслоўе'><?php echo WordParadigmGenerator::showMessage('adverb'); ?></option>
							<option value='прыназоўнік'><?php echo WordParadigmGenerator::showMessage('preposition'); ?></option>
							<option value='злучнік'><?php echo WordParadigmGenerator::showMessage('conjunction'); ?></option>
							<option value='часціца'><?php echo WordParadigmGenerator::showMessage('particle'); ?></option>
							<option value='выклічнік'><?php echo WordParadigmGenerator::showMessage('interjection'); ?></option>
						</select>
					</div>
				</div>
				<div class="col-md-12">
					<button type="button" id="MainButtonId" name="MainButton" class="button-primary"><?php echo WordParadigmGenerator::showMessage('button'); ?></button>
				</div>
				<div class="col-md-12" id="resultBlockId" style="display: none;">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title"><?php echo WordParadigmGenerator::showMessage('result'); ?></h3>
						</div>
						<div class="panel-body">
							<p id="resultId"></p>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-body">
								<?php echo WordParadigmGenerator::showMessage('service code'); ?>&nbsp;<a href="https://gitlab.com/ssrlab1/WordParadigmGenerator" target="_blank"><?php echo WordParadigmGenerator::showMessage('reference'); ?></a>.
								<br />
								<a href="https://gitlab.com/ssrlab1/WordParadigmGenerator/-/issues/new" target="_blank"><?php echo WordParadigmGenerator::showMessage('suggestions'); ?></a>.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<footer class="footer">
			<div class="container">
				<p class="text-muted">
					<?php echo WordParadigmGenerator::showMessage('contact e-mail'); ?>
					<a href="mailto:corpus.by@gmail.com">corpus.by@gmail.com</a>.<br />
					<?php echo WordParadigmGenerator::showMessage('other prototypes'); ?>
					<a href="https://corpus.by/?lang=<?php echo $lang; ?>">corpus.by</a>,&nbsp;<a href="https://ssrlab.by">ssrlab.by</a>.
				</p>
				<p class="text-muted">
					<?php echo WordParadigmGenerator::showMessage('laboratory'), ', ', $ini['year']; if($ini['year'] !== date('Y')) echo '—', date('Y'); ?>
				</p>
			</div>
		</footer>
	</body>
</html>
<?php WordParadigmGenerator::sendErrorList($lang); ?>