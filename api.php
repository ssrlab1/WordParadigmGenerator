<?php
	header("Content-type: text/html;  charset=utf-8");
	header("Access-Control-Allow-Origin: *");
	mb_internal_encoding('UTF-8');
	mb_regex_encoding('UTF-8');
		
	$localization = isset($_POST['localization']) ? $_POST['localization'] : 'en';
	$text = isset($_POST['text']) ? $_POST['text'] : 'абмакванне абмакваннямі абмакванню';
	$mode = isset($_POST['mode']) ? $_POST['mode'] : 'general';
	$tag = isset($_POST['tag']) ? $_POST['tag'] : 'N*';
	$category = isset($_POST['category']) ? $_POST['category'] : 'усе';
	
	include_once 'WordParadigmGenerator.php';
	WordParadigmGenerator::loadLocalization($localization);
	
	$msg = '';
	if(!empty($text))
	{
		$WordParadigmGenerator = new WordParadigmGenerator($text, $mode);
		$WordParadigmGenerator->setTag($tag);
		$WordParadigmGenerator->setCategory($category);
		$WordParadigmGenerator->run();
		$WordParadigmGenerator->saveLogFiles();

		$result['text'] = $text;
		$result['result'] = $WordParadigmGenerator->getResult();
		$msg = json_encode($result);
	}
	echo $msg;
?>