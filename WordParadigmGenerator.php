<?php
	include_once dirname(dirname(__FILE__)) . '/TextToSpeechSynthesizer/TextProcessor.php';
	
	class WordParadigmGenerator
	{
		private static $localizationArr = array();
		private static $localizationErr = array();
		private $text = '';
		private $mode = 0;
		private $mysqli;
		private $tag = '';
		private $category = '';
		private $result = '';
		private $properties = array();
		private $noojFlexionsDictionary = array();
		private $noojFlexionsDictionary2 = array();
		const LETTERS_CYR = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдежзийклмнопрстуфхцчшщъыьэюяЂЃѓЉЊЌЋЏђљњќћџЎўЈҐЁЄЇІіґёєјЅѕї";
		const LETTERS_BEL = "АБВГДЕЁЖЗІЙКЛМНОПРСТУЎФХЦЧШЫЬЭЮЯабвгдеёжзійклмнопрстуўфхцчшыьэюя";
		const LETTERS_LAT = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		const APOSTROPHES = "'ʼ’";
		const BR = "<br>\n";

		function __construct($text = '', $mode = 'general')
		{
			$this->text = $text;
			$this->mode = $mode;
			$this->mysqli = TextProcessor::mysqlConnect();
		}

		function __destruct()
		{
			TextProcessor::mysqlDisconnect($this->mysqli);
		}
		
		public static function loadLanguages()
		{
			$languages = array();
			$files = scandir('lang/');
			if(!empty($files))
			{
				foreach($files as $file)
				{
					if(substr($file, 2, 4) == '.txt')
					{
						$languages[] = substr($file, 0, 2);
					}
				}
			}
			return $languages;
		}
		
		public static function loadLocalization($lang)
		{
			$filepath = "lang/$lang.txt";
			$linesArr = file($filepath, FILE_IGNORE_NEW_LINES);
			if($linesArr === false)
			{
				$filepath = "lang/en.txt";
				$linesArr = file($filepath, FILE_IGNORE_NEW_LINES);
			}
			foreach($linesArr as $line)
			{
				if(empty($line))
				{
					$key = $value = '';
				}
				elseif(substr($line, 0, 1) !== '#' && substr($line, 0, 2) !== '//')
				{
					if(empty($key))
					{
						$key = $line;
					}
					else
					{
						if(!isset(self::$localizationArr[$key]))
						{
							self::$localizationArr[$key] = $line;
						}
					}
				}
			}
		}
		
		public static function showMessage($msg)
		{
			if(isset(self::$localizationArr[$msg]))
			{
				return self::$localizationArr[$msg];
			}
			else
			{
				self::$localizationErr[] = $msg;
				return $msg;
			}
		}
		
		public static function sendErrorList($lang)
		{
			if(!empty(self::$localizationErr))
			{
				$ip = str_replace('.', '-', $_SERVER['REMOTE_ADDR']);
				$sendersName = 'Word Paradigm Generator';
				$recipient = 'corpus.by@gmail.com';
				$subject = "ERROR: Incorrect localization in $sendersName by user $ip";
				$mailBody = 'Вітаю, гэта corpus.by!' . self::BR;
				$mailBody .= $_SERVER['HTTP_HOST'] . '/WordParadigmGenerator/ дасылае інфармацыю аб наступных памылках:' . self::BR . self::BR;
				$mailBody .= "Мова лакалізацыі: <b>$lang</b>" . self::BR;
				$mailBody .= 'Адсутнічае лакалізацыя наступных ідэнтыфікатараў:' . self::BR . implode(self::BR, self::$localizationErr) . self::BR;
				$header  = "MIME-Version: 1.0\r\n";
				$header .= "Content-type: text/html; charset=utf-8\r\n";
				$header .= "From: $sendersName <corpus.by@gmail.com>\r\n";
				mail($recipient, $subject, $mailBody, $header);
			}
		}
		
		public function setText($text)
		{
			$this->text = $text;
		}

		public function setTag($tag)
		{
			$this->tag = $tag;
		}
		
		public function setCategory($category)
		{
			$this->category = $category;
		}
		
		private function readNoojFlexions()
		{
			$this->properties = array();
			$filePath = dirname(__FILE__) . '/base/propertiesForWPG.def';
			$handle = fopen($filePath, 'r') OR die('fail open "propertiesForWPG.def"');
			if($handle)
			{
				$partOfSpeech = '';
				$propertyName = '';
				$propertyArr = array();
				while(($buffer = fgets($handle, 4096)) !== false)
				{
					if(empty($buffer) OR substr($buffer, 0, 1) == '#' OR substr($buffer, 0, 2) == '//')
					{
						continue;
					}
					else
					{
						$splitedLine = explode('=', $buffer);
						if(isset($splitedLine[0]))
						{
							if(strpos($splitedLine[0], '_') !== false)
							{
								$splitedLeftPart = explode('_', $splitedLine[0]);
								$partOfSpeech = trim($splitedLeftPart[0]);
								$propertyName = trim($splitedLeftPart[1]);
							}
							else
							{
								$partOfSpeech = trim($splitedLine[0]);
							}
						}
						if(isset($splitedLine[1]) && strpos($splitedLine[1], ';') !== false)
						{
							$splitedRightPart = explode(';', $splitedLine[1]);
							$propertyArr = explode('+', $splitedRightPart[0]);
							foreach($propertyArr as &$property)
							{
								$property = trim($property);
								$this->properties[$partOfSpeech][] = $property;
							}
						}
					}
				}
			}
			
			$filePath = dirname(__FILE__) . '/base/generalBeForWPG.nof';
			$handle = fopen($filePath, 'r') OR die('fail open "generalBeForWPG.nof"');
			if($handle)
			{
				$baseWord = '';
				while(($buffer = fgets($handle, 4096)) !== false)
				{
					if(empty($buffer) or substr($buffer, 0, 1) == '#' or substr($buffer, 0, 2) == '//')
					{
						continue;
					}
					elseif(substr($buffer, 0, 2) == '<B' or substr($buffer, 0, 3) == '+<B' or substr($buffer, 0, 4) == '+ <B')
					{
						preg_match('/\+? ?<B(\d)>([^\/]*)\/(.*)/u', $buffer, $matches);
						if(isset($matches[1]) && isset($matches[2]) && isset($matches[3]))
						{
							$baseWordTrimed = trim($baseWord, "0123456789");
							$wordBeginning = mb_substr($baseWordTrimed, 0, intval('-' . $matches[1]), 'UTF-8');
							$word = mb_strtolower($wordBeginning . $matches[2], 'UTF-8');
							$this->noojFlexionsDictionary[$word][] = array($baseWord, $matches[3]);
							$this->noojFlexionsDictionary2[$baseWord][] = $buffer;
						}
					}
					elseif(substr($buffer, 0, 2) == '<E' or substr($buffer, 0, 3) == '+<E' or substr($buffer, 0, 4) == '+ <E')
					{
						preg_match('/\+? ?<E>([^\/]*)\/(.*)/u', $buffer, $matches);
						if(isset($matches[1]) && isset($matches[2]))
						{
							$baseWordTrimed = trim($baseWord, "0123456789");
							$word = mb_strtolower($baseWordTrimed . $matches[1], 'UTF-8');
							$this->noojFlexionsDictionary[$word][] = array($baseWord, $matches[2]);
							$this->noojFlexionsDictionary2[$baseWord][] = $buffer;
						}
					}
					else
					{
						if(strpos($buffer, '=') !== false)
						{
							$bufferArr = explode(' ', $buffer);
							$baseWord = $bufferArr[0];
							$this->noojFlexionsDictionary2[$baseWord] = array();
						}
					}
				}
			}
			fclose($handle);
		}
		
		public function run()
		{
			$wordsArr = preg_split("/\s+/", $this->text);
			$wordsCnt = count($wordsArr);
			if($this->mode == 'general')
			{
				foreach($wordsArr as $request)
				{
					$request = trim($request);
					if(empty($request)) continue;
					if($this->checkRequestAllowability($request, $this->mode))
					{
						$paradigmIdArr = array();
						$paradigmListArr = array();	
						$query = $this->formQuery($request, 'word');					
						if($result = $this->mysqli->query($query))
						{
							// калі парадыгма слова знойдзена ў слоўніку
							if($result->num_rows > 0)
							{
								while($row = $result->fetch_assoc())
								{
									$paradigmIdArr[$row['lexemeId']] = 1;
								}
								$result->free();
								foreach($paradigmIdArr as $lexemeId => $v)
								{
									$paradigmCur = '';
									$query = sprintf("SELECT * FROM sbm1987 WHERE lexemeId=%d", $lexemeId);
									if($result = $this->mysqli->query($query))
									{
										while($paradigmRow = $result->fetch_assoc())
										{
											$paradigmCur .= $paradigmRow['accent'] . '_' . $paradigmRow['tag'] . '<br />';
										}
										$paradigmListArr[] = $paradigmCur;
										$result->free();
									}
								}
								$this->result .= '<i>#' . $this->showMessage('paradigm found') . '</i><br />';
								$this->result .= implode('=<br />', $paradigmListArr);
							}
							// калі парадыгма слова не знойдзена ў слоўніку
							else
							{
								$wordTmp = $request;
								$beginning = '';
								$ending = '';
								while(!empty($wordTmp))
								{
									$query = $this->formQuery($wordTmp, 'pattern');
									if($result = $this->mysqli->query($query))
									{
										if($result->num_rows == 0)
										{
											mb_internal_encoding('UTF-8');
											$beginning .= mb_substr($wordTmp, 0, 1);
											$wordTmp = mb_substr($wordTmp, 1);
											continue;
										}
										elseif($result->num_rows > 0)
										{
											$ending = $wordTmp;
											$baseWordArr = array();
											while($row = $result->fetch_assoc())
											{
												$paradigmIdArr[$row['lexemeId']] = $row['accent'];
												$baseWordArr[] = $row['word'];
											}
											$result->free();
											foreach($paradigmIdArr as $lexemeId => $wordOld)
											{
												$paradigmCur = '';
												$beginningOld1 = str_replace($ending, '', $wordOld);
												$wordOld = str_replace('=', '', $wordOld);
												$wordOld = str_replace('+', '', $wordOld);
												$beginningOld2 = str_replace($ending, '', $wordOld);
												$query = sprintf("SELECT * FROM sbm1987 WHERE lexemeId=%d", $lexemeId);
												if($result = $this->mysqli->query($query))
												{
													while($paradigmRow = $result->fetch_assoc())
													{
														mb_internal_encoding('UTF-8');
														mb_regex_encoding('UTF-8');
														$len1 = mb_strlen($beginningOld1);
														$len2 = mb_strlen($beginningOld2);
														if(mb_substr($paradigmRow['accent'], 0, $len2, 'UTF-8') == $beginningOld2)
														{
															$endingCur = mb_substr($paradigmRow['accent'], $len2);
														}
														elseif(mb_substr($paradigmRow['accent'], 0, $len1, 'UTF-8') == $beginningOld1)
														{
															$endingCur = mb_substr($paradigmRow['accent'], $len1);
														}
														else
														{
															$endingCur = $paradigmRow['accent'];
														}
														
														$wordformTmp = str_replace('=', '', $beginning . $endingCur);
														$wordformTmp = str_replace('+', '', $wordformTmp);
														
														if($request == $wordformTmp) {
															$paradigmCur .= '<b>' . $beginning . $endingCur . '</b>_' . $paradigmRow['tag'] . '<br />';
														}
														else {
															$paradigmCur .= $beginning . $endingCur . '_' . $paradigmRow['tag'] . '<br />';
														}
													}
													$result->free();
													$key = array_search($paradigmCur, $paradigmListArr, true);
													if($key === false)
													{
														$paradigmListArr[$wordOld] = $paradigmCur;
													}
													else
													{
														unset($paradigmListArr[$key]);
														$key = $key . ', ' . $wordOld;
														$paradigmListArr[$key] = $paradigmCur;
													}
												}
											}
											foreach($paradigmListArr as $baseWords => $paradigm)
											{
												$this->result .= '<i>#' . $this->showMessage('paradigm generated') . ': <b>' . $baseWords . '</b></i><br />';
												$this->result .= $paradigm . '<br />';
											}
											break;
										}
									}
								}
							}
						}
					}
					else
					{
						$this->result .= $request . ' – ' . $this->showMessage('uncorrect request') . '<br>' . $this->showMessage('request example') . ': <i>сонца</i><br />';
					}
				}
			}
				
			if($this->mode == 'nooj')
			{
				foreach($wordsArr as $request)
				{
					if(empty($request)) continue;
					$request = trim($request);			
					if($this->checkRequestAllowability($request, $this->mode))
					{
						$this->readNoojFlexions();
						$requestArr = explode(',', $request);
						$requestWord = $requestArr[0];
						$partOfSpeech = $requestArr[1];
						
						$requestWord = mb_strtolower($requestWord, 'UTF-8');
						if(array_key_exists($requestWord, $this->noojFlexionsDictionary))
						{
							foreach($this->noojFlexionsDictionary[$requestWord] as $tmp)
							{
								$resultTmp = $requestWord . ',' . $partOfSpeech . '+FLX=' . $tmp[0] . '<br />';
								if(isset($this->noojFlexionsDictionary2[$tmp[0]]))
								{
									$k = '';
									$k2 = '';
									foreach($this->noojFlexionsDictionary2[$tmp[0]] as $tmp2)
									{
										if(preg_match('/\+? ?<B(\d)>([^\/]*)\/(.*)/u', $tmp2, $matches) === 1)
										{
											if(isset($matches[1]) && isset($matches[2]) && isset($matches[3]))
											{
												$wordBeginning = mb_substr($requestWord, 0, intval('-' . $matches[1]), 'UTF-8');
												$wordNew = mb_strtolower($wordBeginning . $matches[2], 'UTF-8');
												$propertiesCurrent = explode('+', $matches[3]);
												foreach($propertiesCurrent as $propertyCurrent)
												{
													$propertyCurrent = trim($propertyCurrent, ';');
													if(isset($this->properties[$partOfSpeech]))
													{
														if(!in_array($propertyCurrent, $this->properties[$partOfSpeech]))
														{
															break 2;
														}
													}
													else
													{
														$this->result .= $request . ' – ' . $this->showMessage('uncorrect request') . '<br>' . $this->showMessage('request example') . ': <i>сонца,NOUN</i><br />';
														break 2;
													}
												}
												$k .= $wordNew . '/' . $matches[3] . '<br />';
											}
										}
										elseif(preg_match('/\+? ?<E>([^\/]*)\/(.*)/u', $tmp2, $matches) === 1)
										{
											if(isset($matches[1]) && isset($matches[2]))
											{
												$wordNew = mb_strtolower($requestWord . $matches[1], 'UTF-8');
												$propertiesCurrent = explode('+', $matches[2]);
												foreach($propertiesCurrent as $propertyCurrent)
												{
													$propertyCurrent = trim($propertyCurrent, ';');
													if(isset($this->properties[$partOfSpeech]))
													{
														if(!in_array($propertyCurrent, $this->properties[$partOfSpeech]))
														{
															break 2;
														}
													}
													else
													{
														$this->result .= $request . ' – ' . $this->showMessage('uncorrect request') . '<br>' . $this->showMessage('request example') . ': <i>сонца,NOUN</i><br />';
														break 2;
													}
												}
												$k .= $wordNew . '/' . $matches[2] . '<br />';
											}
										}
										$k2 .= htmlspecialchars($tmp2) . '<br />';
									}
									$resultTmp .= $k;
									$resultTmp .= $tmp[0] . ' =<br />';
									$resultTmp .= $k2;
									$resultTmp .= '<br />';
								}
								$this->result .= $resultTmp;
							}
						}
						else
						{
							$ending = '';
							$shortenedWord = $requestWord;
							$keysResult = array();
							while(!empty($shortenedWord))
							{
								foreach($this->noojFlexionsDictionary as $wordform => $value)
								{
									if(preg_match("/$shortenedWord$/u", $wordform))
									{
										$keysResult[$value[0][0]][$wordform] = 1;
									}
								}
								mb_internal_encoding('UTF-8');
								$shortenedWord = mb_substr($shortenedWord, 1);
							}
							if(!empty($keysResult))
							{
								foreach($keysResult as $baseWord => $wordformArr)
								{
									foreach($wordformArr as $wordform => $v)
									{
										foreach($this->noojFlexionsDictionary[$wordform] as $tmp)
										{
											$resultTmp = $requestWord . ',' . $partOfSpeech . '+FLX=' . $tmp[0] . '<br />';
											if(isset($this->noojFlexionsDictionary2[$tmp[0]]))
											{
												$k = '';
												$k2 = '';
												foreach($this->noojFlexionsDictionary2[$tmp[0]] as $tmp2)
												{
													if(preg_match('/\+? ?<B(\d)>([^\/]*)\/(.*)/u', $tmp2, $matches) === 1)
													{
														if(isset($matches[1]) && isset($matches[2]) && isset($matches[3]))
														{
															$wordBeginning = mb_substr($requestWord, 0, intval('-' . $matches[1]), 'UTF-8');
															$wordNew = mb_strtolower($wordBeginning . $matches[2], 'UTF-8');
															$propertiesCurrent = explode('+', $matches[3]);
															foreach($propertiesCurrent as $propertyCurrent)
															{
																$propertyCurrent = trim($propertyCurrent, ';');
																if(isset($this->properties[$partOfSpeech]))
																{
																	if(!in_array($propertyCurrent, $this->properties[$partOfSpeech]))
																	{
																		break 2;
																	}
																}
																else
																{
																	$this->result .= $request . ' – ' . $this->showMessage('uncorrect request') . '<br>' . $this->showMessage('request example') . ': <i>сонца,NOUN</i><br />';
																	break 2;
																}
															}
															$k .= $wordNew . '/' . $matches[3] . '<br />';
														}
													}
													elseif(preg_match('/\+? ?<E>([^\/]*)\/(.*)/u', $tmp2, $matches) === 1)
													{
														if(isset($matches[1]) && isset($matches[2]))
														{
															$wordNew = mb_strtolower($requestWord . $matches[1], 'UTF-8');
															$propertiesCurrent = explode('+', $matches[2]);
															foreach($propertiesCurrent as $propertyCurrent)
															{
																$propertyCurrent = trim($propertyCurrent, ';');
																if(isset($this->properties[$partOfSpeech]))
																{
																	if(!in_array($propertyCurrent, $this->properties[$partOfSpeech]))
																	{
																		break 2;
																	}
																}
																else
																{
																	$this->result .= $request . ' – ' . $this->showMessage('uncorrect request') . '<br>' . $this->showMessage('request example') . ': <i>сонца,NOUN</i><br />';
																	break 2;
																}
															}
															$k .= $wordNew . '/' . $matches[2] . '<br />';
														}
													}
													
													$k2 .= htmlspecialchars($tmp2) . '<br />';
												}
												$resultTmp .= $k;
												$resultTmp .= $tmp[0] . ' =<br />';
												$resultTmp .= $k2;
												$resultTmp .= '<br />';
											}
											$this->result .= $resultTmp;
										}
									}
								}
							}
						}
					}
					else
					{
						$this->result .= $request . ' – ' . $this->showMessage('uncorrect request') . '<br>' . $this->showMessage('request example') . ': <i>сонца,NOUN</i><br />';
					}
				}

				$arrrrrrr = explode('<br /><br />', $this->result);
				foreach($arrrrrrr as $arrrrrr)
				{
					$cnt = 0;
					$edited = $arrrrrr;
					foreach($wordsArr as $request)
					{
						$requestArr = explode(',', $request);
						$requestWord = mb_strtolower($requestArr[0], 'UTF-8');
						if(strpos($arrrrrr, $requestWord . '/') !== false)
						{
							$edited = str_replace($requestWord . '/', '<b>' . $requestWord . '/</b>', $edited);
							$cnt++;
						}
					}
					if($cnt)
					{
						$resultArr[$cnt][] = $edited;
					}
				}

				$this->result = '';
				for($i = $wordsCnt; $i>0; $i--)
				{
					if(!empty($resultArr[$i]))
					{
						$uniqueResultArr = array_unique($resultArr[$i]);
						if($i != 1)
						{
							$this->result .= "<br><br><b>" . $this->showMessage('paradigm source 1') . " $i " . $this->showMessage('paradigm source 2') . " (" . $this->showMessage('total') . " " . count($uniqueResultArr) . "):</b><br><br>";
						}
						elseif($i == 1)
						{
							$this->result .= "<br><br><b>" . $this->showMessage('paradigm source 1') . " $i " . $this->showMessage('paradigm source 3') . " (" . $this->showMessage('total') . " " . count($uniqueResultArr) . "):</b><br><br>";
						}
						foreach($uniqueResultArr as $uniqueResult)
						{
							$this->result .= $uniqueResult . '<br /><br />';
						}
					}
				}
			}
		}
		
		private function checkRequestAllowability($request, $mode)
		{
			if($mode == 'general')
			{
				$pattern = "/^[" . self::LETTERS_BEL . self::APOSTROPHES . "\-]+$/u";
				if(preg_match($pattern, $request) === 1)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			if($mode == 'nooj')
			{
				$pattern = "/^[" . self::LETTERS_BEL . self::APOSTROPHES . "\-]+,[" . self::LETTERS_LAT . "]+$/u";
				if(preg_match($pattern, $request) === 1)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
		}
		
		private function formQuery($request, $mode)
		{
			$queryAddition = '';
			if(!empty($this->tag))
			{
				$this->tag = str_replace('*', '%', $this->tag);
				$this->tag = str_replace('.', '_', $this->tag);
				$queryAddition = "AND tag LIKE '" . $this->tag . "'";
			}
			elseif($this->category != 'усе')
			{
				$tagList = $this->getTagListByCategory($this->category);
				$queryAddition = "AND tag IN $tagList";
			}
			$requestWithSlashes = addslashes($request);
			if($mode == 'word')
			{
				$query = sprintf("SELECT * FROM sbm1987 WHERE word='%s' %s", $requestWithSlashes, $queryAddition);
			}
			if($mode == 'pattern')
			{
				$query = sprintf("SELECT * FROM sbm1987 WHERE word LIKE '%s' %s", '%' . $requestWithSlashes, $queryAddition);
			}
			return $query;
		}
		
		private function getTagListByCategory($category)
		{
			$category = addslashes($category);
			$query = sprintf("SELECT tag FROM tags WHERE category = '%s'", $category);
			if($result = $this->mysqli->query($query))
			{
				while($row = $result->fetch_assoc())
				{
					$tagsArr[] = '\'' . $row['tag'] . '\'';
				}
				$result->free();
			}
			return '(' . implode(',', $tagsArr) . ')';
		}
		
		private function mbStrToUpperFirst($str, $encoding = 'UTF8')
		{
			return
				mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding) .
				mb_substr($str, 1, mb_strlen($str, $encoding), $encoding);
		}
		
		public function saveLogFiles()
		{
			mb_internal_encoding('UTF-8');
			mb_regex_encoding('UTF-8');
			
			$dateCode = date('Y-m-d_H-i-s', time());
			$randCode = rand(0, 1000);
			$ip = str_replace('.', '-', $_SERVER['REMOTE_ADDR']);
			
			$root = $_SERVER['HTTP_HOST'];
			$serviceName = 'WordParadigmGenerator';
			$sendersName = 'Word Paradigm Generator';
			$sendersEmail = 'corpus.by@gmail.com';
			$recipient = 'corpus.by@gmail.com';
			$subject = "Word Paradigm Generator from IP $ip";
			$mailBody = "Вітаю, гэта corpus.by!" . self::BR;
			$mailBody .= "$root/$serviceName/ дасылае інфармацыю аб актыўнасці карыстальніка з IP $ip." . self::BR . self::BR;
			$textLength = mb_strlen($this->text);
			$pages = round($textLength/2300, 1);
			
			$cachePath = dirname(dirname(__FILE__)) . "/_cache";
			if(!file_exists($cachePath)) mkdir($cachePath);
			$cachePath = "$cachePath/$serviceName";
			if(!file_exists($cachePath)) mkdir($cachePath);
			
			$filename = $dateCode . '_'. $ip . '_' . $randCode . '_in.txt';
			$path = "$cachePath/in/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$newFile = fopen($filepath, 'wb') OR die('open cache file error');
			$cacheText = preg_replace("/(^\s+)|(\s+$)/us", "", $this->text);
			fwrite($newFile, $cacheText);
			fclose($newFile);
			$url = $root . "/showCache.php?s=WordParadigmGenerator&t=in&f=$filename";
			if(mb_strlen($cacheText))
			{
				$mailBody .= "Тэкст ($textLength сімв., прыкладна $pages ст. па 2300 сімв. на старонку) пачынаецца з:" . self::BR;
				preg_match('/([^\n]*\n?){1,3}/u', $cacheText, $matches);
				$str = str_replace("\n", self::BR, trim($matches[0]));
				if(mb_strlen($str) < 300)
					$mailBody .= '<blockquote><i>' . $str . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
				else
					$mailBody .= '<blockquote><i>' . mb_substr($str, 0, 300) . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
			}
			$mailBody .= self::BR;
			
			$filename = $dateCode . '_'. $ip . '_' . $randCode . '_out.txt';
			$path = "$cachePath/out/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$newFile = fopen($filepath, 'wb') OR die('open cache file error');
			$cacheText = preg_replace("/(^\s+)|(\s+$)/us", "", $this->result);
			fwrite($newFile, $cacheText);
			fclose($newFile);
			$url = $root . "/showCache.php?s=WordParadigmGenerator&t=out&f=$filename";
			if(mb_strlen($cacheText))
			{
				$mailBody .= "Вынік пачынаецца з:" . self::BR;
				preg_match('/([^\n]*\n?){1,3}/u', $cacheText, $matches);
				$mailBody .= '<blockquote><i>' . trim($matches[0]) . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
			}
			$mailBody .= self::BR;
			
			$header  = "MIME-Version: 1.0\r\n";
			$header .= "Content-type: text/html; charset=utf-8\r\n";
			$header .= "From: $sendersName <$sendersEmail>\r\n";
			mail($recipient, $subject, $mailBody, $header);
			
			$filename = $dateCode . '_' . $ip . '_' . $randCode . '_e.txt';
			$path = "$cachePath/email/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$newFile = fopen($filepath, 'wb') OR die('open cache file error');
			fwrite($newFile, join("\n", array("recipient: \t" . $recipient, "subject: \t" . $subject, "\n\tMAIL BODY\n\n" . $mailBody, $header)));
			fclose($newFile);
		}
		
		public function getResult()
		{
			return $this->result;
		}
		
		public function getParagraphCnt()
		{
			return $this->wordsCnt;
		}
	}
?>